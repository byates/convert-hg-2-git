#!/usr/bin/python3

#############################################################
# Copyright 2020 Brent Yates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#############################################################

import sys
import os
import argparse
import glob
import shutil
from pprint import pprint
import tempfile
import subprocess
import re
import json
from requests_oauthlib import OAuth2Session

BASE_API_URL = "https://api.bitbucket.org/2.0/"

Verbose = False


class SmartFormatter(argparse.HelpFormatter):
    """
    Class to override how argparse handles help messages to support multiple lines.
    """

    def _split_lines(self, text, width):
        if '\n' in text:
            return text.splitlines()
        # this is the RawTextHelpFormatter._split_lines
        return argparse.HelpFormatter._split_lines(self, text, width)


def readCredentials(path):
    """ Read the key:value pairs from a credentials file

    Args:
        path (string) : location of the file
    Returns:
        Dictionary of key:value pairs read from the file
    """
    d = {}
    with open(path, 'r') as f:
        for line in f:
            (key, _, val) = line.strip().partition(":")
            key = key.strip()
            val = val.strip()
            d[key] = val
    return d


def startBBSession(consumer_key, consumer_secret):
    """Starts a oauth session to Bitbucket

    Args:
        consumer_key (string) : oauth consumer key
        consumer_secret (string) : oauth consumer secret
    Returns:
        Tuple of (OAuth2Session, OAuth2Token)
    """
    from requests.auth import HTTPBasicAuth
    from oauthlib.oauth2 import BackendApplicationClient
    auth = HTTPBasicAuth(consumer_key, consumer_secret)
    client = BackendApplicationClient(client_id=consumer_key)
    oauth = OAuth2Session(client=client)
    token = oauth.fetch_token(
        token_url='https://bitbucket.org/site/oauth2/access_token', auth=auth)
    return (oauth, token)


def IsRemoteRepo(string):
    return string.startswith('ssh://') or string.startswith('https://')


def createRepo(sess, path, workspace, slug, rp):
    Name = rp['name']
    ProjectKey = rp['project']['key']
    d = {
        "name": Name,
        "scm": "git",
        "fork_policy": rp['fork_policy'],
        "is_private": rp['is_private'],
        "language": rp['language'],
        "project": {'key': ProjectKey},
        "description": rp['description']
    }
    url = BASE_API_URL + \
        'repositories/{ws}/{slug}'.format(ws=workspace, slug=slug)
    rt = sess.post(url=url, json=d).json()
    return(rt)


def getWorkspaces(sess):
    """Returns a list of all workspaces for the current session.

    Args:
        sess (OAuth2Session): Session object
    Returns:
        List of all workspace slug values (strings)
    """
    l = []
    url = BASE_API_URL + 'workspaces'
    while True:
        rt = sess.get(url).json()
        for v in rt['values']:
            l.append(v['slug'])
        if not 'next' in rt:
            break
        url = rt['next']
    return l


def getRepos(sess, workspace, role=""):
    """Returns a list of all repos in the workspace.
    Each entry is a repo dictionary object

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Name of the target workspace
        role (string)       : Optional role filter (admin, contributor, member, owner)
    Returns:
        List of all repos in the workspace
    """
    l = []
    url = BASE_API_URL + 'repositories/'+workspace
    if role != "":
        url = url + "?role=" + role
    while True:
        rt = sess.get(url).json()
        for v in rt['values']:
            l.append(v)
        if not 'next' in rt:
            break
        url = rt['next']
    return l


def getBranches(sess, workspace, slug):
    """Builds a list all branches in a repo

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Name of the target workspace
        slug (string) : Slug name of the target repository
    Returns:
        List of all branches
    """
    l = []
    url = BASE_API_URL + \
        'repositories/{ws}/{slug}/refs/branches'.format(
            ws=workspace, slug=slug)
    while True:
        rt = sess.get(url).json()
        for v in rt['values']:
            if v['type'] == 'named_branch':
                l.append(v['name'])
                if len(v['heads']) > 1:
                    print("WARNING: Repo {}:{} branch {} has multiple heads.".format(
                        workspace, slug, v['name']))
        if not 'next' in rt:
            break
        url = rt['next']
    return l


def getRepoProperties(sess, workspace, slug):
    """Returns a dictionary of all repository properties

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Name of the target workspace
        slug (string) : Slug name of the target repository
    Returns:
        Dictionary containing all the repository properties
    """
    url = BASE_API_URL + \
        'repositories/{ws}/{slug}'.format(ws=workspace, slug=slug)
    rt = sess.get(url).json()
    return rt


def setRepoProp(sess, workspace, slug, name, value):
    """Updates the value of a single repo property

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Name of the target workspace
        slug (string) : Slug name of the target repository
        name (string) : Name of the property to update
        value (string) : New value for the property
    Returns:
        Dictionary containing all the repository properties after the update
    """
    data = {name: value}
    url = BASE_API_URL + \
        'repositories/{ws}/{slug}'.format(ws=workspace, slug=slug)
    rt = sess.put(url=url, data=data).json()
    return rt


def renameRepo(sess, workspace, slug, new_name):
    """Helper function to change the name of a repo

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Name of the target workspace
        slug (string) : Slug name of the target repository
        new_name (string) : New name of the repository
    Returns:
        Dictionary containing all the repository properties after the update
    """
    rt = setRepoProp(sess, workspace, slug, 'name', new_name)
    return rt


def listAllWorkspaces(sess):
    """Prints a list of all workspaces found to stdout

    Args:
        sess (OAuth2Session): Session object
    Returns:
        none
    """
    ws = getWorkspaces(sess)
    if len(ws) == 0:
        print("ERROR: no workspaces found")
        return
    for w in ws:
        print(w)


def listAllRepos(sess, workspace, role=""):
    """Prints a list of all repos found to stdout

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Optional name of a workspace. If none, all workspaces are used.
        role (string)       : Optional role filter (admin, contributor, member, owner)
    Returns:
        none
    """
    if workspace:
        ws = [workspace]
    else:
        ws = getWorkspaces(sess)
    repo_count = 0
    for w in ws:
        rp = getRepos(sess, w, role)
        for r in rp:
            Name = r['name']
            Slug = r['slug']
            Scm = r['scm']
            print("[{:2}] {} : {} - slug({}) - scm({})".format(
                repo_count, w, Name, Slug, Scm))
            repo_count = repo_count + 1
    if repo_count == 0:
        print("ERROR: no repositories found")


def getAllHgRepos(sess, workspace):
    """Returns a list of all Mercurial repos

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Optional name of a workspace. If none, all workspaces are used.
    Returns:
        none
    """
    if workspace:
        ws = [workspace]
    else:
        ws = getWorkspaces(sess)
    l = []
    for w in ws:
        rp = getRepos(sess, w, 'admin')
        for r in rp:
            if (r['scm'] == 'hg') and (not r['name'].startswith('hg_')):
                l.append(r)
    return(l)


def cloneRepo(path, workspace, slug):
    url = "ssh://hg@bitbucket.org/"+workspace+"/"+slug
    if Verbose:
        subprocess.check_call(
            ['hg', 'clone', url, path])
    else:
        subprocess.check_call(
            ['hg', 'clone', url, path], stdout=subprocess.DEVNULL)


def getHgBookmarks(path):
    #oldpwd = os.getcwd()
    # os.chdir(path)
    output = subprocess.check_output(
        ['hg', 'bookmarks', '-T', '{bookmark}\\n'], cwd=path).decode(sys.stdout.encoding).strip()
    Bookmarks = output.splitlines()
    Bookmarks = [x.strip() for x in Bookmarks]
    # os.chdir(oldpwd)
    return(Bookmarks)


def getHgBranches(path):
    #oldpwd = os.getcwd()
    # os.chdir(path)
    output = subprocess.check_output(
        ['hg', 'branches', '-T', '{branch}\\n'], cwd=path).decode(sys.stdout.encoding).strip()
    Branches = output.splitlines()
    Branches = [x.strip() for x in Branches]
    # os.chdir(oldpwd)
    return(Branches)


def getGitBranches(path):
    #oldpwd = os.getcwd()
    # os.chdir(path)
    output = subprocess.check_output(
        ['git', 'branch', '-l'], cwd=path).decode(sys.stdout.encoding).strip()
    Branches = output.splitlines()
    Branches = [x.strip() for x in Branches]
    # os.chdir(oldpwd)
    return(Branches)


def renameBookmarksInLocalRepo(path):
    """Change the name of existing bookmarks to '<bookmark>_git'
    """
    Bookmarks = getHgBookmarks(path)
    for m in Bookmarks:
        if Verbose:
            print("Renaming bookmark '{}' to '{}'".format(
                m, m+"_git"))
        subprocess.check_call(
            ['hg', 'bookmark', '-m', m, m + "_git"], cwd=path)


def addBookmarksToLocalRepo(path):
    """Add a bookmark for each existing branch.
    """
    Branches = getHgBranches(path)
    for branch in Branches:
        if Verbose:
            print("Adding bookmark '{}' to branch '{}'".format(
                branch+"_git", branch))
        subprocess.check_call(
            ['hg', 'bookmark', '-r', branch, branch + "_git"], cwd=path)


def createLocalGitRepo(path):
    subprocess.check_call(
        ['git', 'init', path], stdout=subprocess.DEVNULL)


def pushHgRepoToLocalGit(hgPath, gitPath):
    if Verbose:
        subprocess.check_call(
            ['hg', 'push', gitPath], cwd=hgPath)
    else:
        subprocess.check_call(
            ['hg', 'push', gitPath], cwd=hgPath, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def pushRepo(path, dst_workspace, dst_slug):
    url = "ssh://git@bitbucket.org/{}/{}.git".format(
        dst_workspace, dst_slug)
    oldpwd = os.getcwd()
    os.chdir(path)
    if Verbose:
        subprocess.check_call(
            ['git', 'push', url, '--all'], cwd=path)
    else:
        subprocess.check_call(
            ['git', 'push', url, '--all'], cwd=path, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    os.chdir(oldpwd)


def fixupGitBranchNames(path):
    # Branches is the original names. The current names have '_git' suffix.
    Branches = getGitBranches(path)
    oldpwd = os.getcwd()
    os.chdir(path)
    for b in Branches:
        if not b.endswith('_git'):
            continue
        if b == 'default_git':
            subprocess.check_call(
                ['git', 'branch', '-m', b, 'master'])
        else:
            if b == 'master_git':
                continue
            else:
                subprocess.check_call(
                    ['git', 'branch', '-m', b, b[:-4]])
    os.chdir(oldpwd)


def convertRepo(creds, workspace, slug):
    """Converts a single repo from HG to GIT

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : Name of a workspace that holds the repo
        slug (string) : Slug (name) of the repository
    Returns:
        none
    """
    print("Converting {}:{}".format(workspace, slug))
    (BBSess, _) = startBBSession(creds["key"], creds["secret"])
    # Save original source name for later use
    OriginalSourceProps = getRepoProperties(BBSess, workspace, slug)
    BBSess.close()
    HgTempPath = os.path.abspath("./hg_tmp")
    GitTempPath = os.path.abspath("./git_tmp")
    try:
        # Verify the source repo is Mercurial
        if OriginalSourceProps['scm'] != 'hg':
            print("ERROR: Source repo is not a Mercurial repo")
            return
        # Clone the repo to a temporary location
        print("    Cloning {}:{} to {}".format(
            workspace, slug, HgTempPath))
        cloneRepo(HgTempPath, workspace, slug)
        # Assign '{branch}_git' bookmark to all branches in the clone
        print("    Creating bookmarks so hg2git will properly create branches.")
        renameBookmarksInLocalRepo(HgTempPath)
        addBookmarksToLocalRepo(HgTempPath)
        # Create a temp local git repo
        print("    Creating local git repo")
        createLocalGitRepo(GitTempPath)
        # Push local HG repo to local GIT repo
        print("    Pushing HG content to local git repo")
        pushHgRepoToLocalGit(HgTempPath, GitTempPath)
        # Rename branches to remove the `_git` part
        print("    Fixing up branch names in local git repo")
        fixupGitBranchNames(GitTempPath)
    except:
        print("ERROR: conversion failed for {}:{}!".format(workspace, slug))
        print("Unexpected error:", sys.exc_info()[0])
        shutil.rmtree(HgTempPath)
        shutil.rmtree(GitTempPath)
        raise

    try:
        (BBSess, _) = startBBSession(creds["key"], creds["secret"])
        # Rename the source remote repo as 'hg_'+{source_name}
        NewRemoteHgRepoName = 'hg_'+OriginalSourceProps['name']
        print("    Renaming original Hg repo to {}:'{}'".format(
            workspace, NewRemoteHgRepoName))
        renameRepo(BBSess, workspace, slug, NewRemoteHgRepoName)
        # Create a new GIT repo on BitBucket with same name as the source repo
        print("    Creating GIT repo {}:{}".format(workspace, slug))
        GitRepo = createRepo(BBSess, HgTempPath, workspace,
                             slug, OriginalSourceProps)
        BBSess.close()
        # Push local GIT repo to new repo
        print("    Push local repo (git) to remote GIT repo @ {}.{}".format(workspace,
                                                                        GitRepo['slug']))
        pushRepo(GitTempPath, workspace, GitRepo['slug'])
        # Remove local temp repo
        print("    Cleaning up")
        shutil.rmtree(HgTempPath)
        shutil.rmtree(GitTempPath)
        print("    Conversion complete.")
    except:
        print("ERROR: Push to remote repo failed for {}:{}!".format(workspace, slug))
        print("Unexpected error:", sys.exc_info()[0])
        shutil.rmtree(HgTempPath)
        shutil.rmtree(GitTempPath)
        raise


def convertRepos(creds, workspace):
    """Converts every repo in the list from HG to GIT

    Args:
        sess (OAuth2Session): Session object
        workspace (string)  : (optional) Name of a workspace that holds the repo
    Returns:
        none
    """
    (BBSess, _) = startBBSession(creds["key"], creds["secret"])
    l = getAllHgRepos(BBSess, workspace)
    for r in l:
        convertRepo(creds, r['workspace']['slug'], r['slug'])


if __name__ == '__main__':

    Parser = argparse.ArgumentParser(description='Application to convert Bitbucket repos from Mercurial to Git',
                                     formatter_class=SmartFormatter)
    Parser.add_argument('-c', '--credentials', default='.credentials',
                        help='Path to file with oauth key and value')
    Parser.add_argument('-s', '--src_repo',
                        help='Source Mercurial repo (default is any)'),
    Parser.add_argument('-w', '--workspace',
                        help='Specify the workspace filter (default is all)'),
    Parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increases the amount of output messages.')
    Parser.add_argument('cmd', choices=['list-ws', 'list-repos', 'convert-repo', 'convert-all'],
                        help="list-ws   : List all Workspaces\n"
                        "list-repos: List all Mecurial Repos\n"
                        "convert-repo: Convert repo specified by workspace:repo\n"
                        "convert-all: Convert all repos in the workspace or all workspaces"
                        )

    Args = Parser.parse_args()
    Verbose = Args.verbose

    BBCreds = readCredentials(Args.credentials)

    if not Args.cmd:
        print("At least one command directive argument is required. See help.")
        exit(-1)

    if Args.cmd == 'list-ws':
        if Args.workspace or Args.src_repo:
            print("ERROR: 'list-ws' doesn't take any arguments.")
            exit(-1)
        (BBSess, _) = startBBSession(BBCreds["key"], BBCreds["secret"])
        listAllWorkspaces(BBSess)
        exit(0)

    if Args.cmd == 'list-repos':
        if Args.src_repo:
            print("ERROR: 'list-repos' doesn't take the src_repo argument.")
            exit(-1)
        (BBSess, _) = startBBSession(BBCreds["key"], BBCreds["secret"])
        listAllRepos(BBSess, Args.workspace)
        exit(0)

    if Args.cmd == 'convert-repo':
        if (not Args.workspace) or (not Args.src_repo):
            print(
                "ERROR: 'convert-repos' requires both workspace and src_repo arguments.")
            exit(-1)
        convertRepo(BBCreds, Args.workspace, Args.src_repo)
        exit(0)

    if Args.cmd == 'convert-all':
        if Args.src_repo:
            print("ERROR: 'convert-all' doesn't take the src_repo argument.")
            exit(-1)
        convertRepos(BBCreds, Args.workspace)
        exit(0)
