# README #

This script converts a Mercurial Repo in bitbucket to a GIT repo in bitbucket. It was developed for my own repos and is very brittle so test carefully.

run: `convert-hg-to-git.py --help` for usage instructions.

## SETUP ##

### Install HG ###

```
sudo apt install mercurial
cd ~/tools
hg clone https://www.mercurial-scm.org/repo/hg
cd hg
hg checkout 5.4
sudo apt purge mercurial
python3 setup.py install --user
```

### Install Dulwich ###

```
cd ~/tools
git clone https://github.com/dulwich/dulwich
cd dulwich
git checkout dulwich-0.20.0
python3 setup.py install --user
```
### Install hg-git ###

```
cd ~/tools
hg clone https://foss.heptapod.net/mercurial/hg-git
Setup hgrc file
```

```
[ui]
username = First Last <first.last@mail.com>
verbose = True
[extensions]
largefiles =
rebase =
shelve =
strip =
hggit = ~/tools/hg-git/hggit
```

### Check config ###
```
hg version
```

```
Mercurial Distributed SCM (version 5.4)
(see https://mercurial-scm.org for more information)

Copyright (C) 2005-2020 Matt Mackall and others
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Enabled extensions:

  largefiles  internal
  rebase      internal
  strip       internal
  hggit       external  0.9.0a1 (dulwich 0.19.16)

```

### SSH ###

SSH keys must be installed for the script to access the bitbucket servers.

### Consumer Key and Secret ###

Within bitbucket, setup a _consumer_:

```Workspace Setings--> Oauth Consumer```
Put the `key` and the `secret` into a local file called `.credentials` in the following form:
```
key : <key>
secret : <secret>
```